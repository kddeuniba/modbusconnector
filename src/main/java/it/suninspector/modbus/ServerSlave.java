package it.suninspector.modbus;

import net.wimpi.modbus.Modbus;
import net.wimpi.modbus.ModbusCoupler;
import net.wimpi.modbus.net.ModbusTCPListener;
import net.wimpi.modbus.net.ModbusUDPListener;
import net.wimpi.modbus.procimg.SimpleDigitalIn;
import net.wimpi.modbus.procimg.SimpleDigitalOut;
import net.wimpi.modbus.procimg.SimpleInputRegister;
import net.wimpi.modbus.procimg.SimpleProcessImage;
import net.wimpi.modbus.procimg.SimpleRegister;

/**
 * @author fabiofumarola
 *
 *	It represents the slave in modbus architecture. It is waiting for request from masters.
 */
public class ServerSlave {
	
	private ModbusTCPListener listener;
	
	private SimpleProcessImage spi;
	
	private int port = Modbus.DEFAULT_PORT;
	
	public ServerSlave(int port) {
		System.setProperty("net.wimpi.modbus.debug", "true");
		this.port = port;
	}
	
	public void prepareProcessImage(){
		spi = new SimpleProcessImage();
		spi.addDigitalOut(new SimpleDigitalOut(true));
		spi.addDigitalOut(new SimpleDigitalOut(false));
		spi.addDigitalIn(new SimpleDigitalIn(false));
		spi.addDigitalIn(new SimpleDigitalIn(true));
		spi.addDigitalIn(new SimpleDigitalIn(false));
		spi.addDigitalIn(new SimpleDigitalIn(true));
		spi.addRegister(new SimpleRegister(251));
		spi.addInputRegister(new SimpleInputRegister(45));
		
		ModbusCoupler.getReference().setProcessImage(spi);
		ModbusCoupler.getReference().setMaster(false);
		ModbusCoupler.getReference().setUnitID(15);   
	}
	
	public void createListener(int poolSize){
		listener = new ModbusTCPListener(poolSize);
		listener.setPort(port);
		listener.start();
		System.out.println("starting modbus on port " + port);
		System.out.println("is the server listening: "  + listener.isListening());
	}
	
	public static void main(String[] args) {
		
		int port = 5555;
		int poolsize = 3;
		
		ServerSlave server = new ServerSlave(port);
		server.prepareProcessImage();
		server.createListener(3);

	}
	
}
