package it.suninspector.modbus;

import java.net.InetAddress;
import java.net.UnknownHostException;

import net.wimpi.modbus.Modbus;
import net.wimpi.modbus.ModbusException;
import net.wimpi.modbus.ModbusIOException;
import net.wimpi.modbus.ModbusSlaveException;
import net.wimpi.modbus.io.ModbusTCPTransaction;
import net.wimpi.modbus.msg.ReadInputDiscretesRequest;
import net.wimpi.modbus.msg.ReadInputDiscretesResponse;
import net.wimpi.modbus.net.TCPMasterConnection;

/**
 * @author fabiofumarola
 * this is the client that is the master in the modbus architecture. It starts the communication
 * by sending a request to the server.
 */
public class ClientMaster {
	
	private InetAddress addr = null; //the slave's address
	private int port = Modbus.DEFAULT_PORT;
	private int ref = 0; //the reference; offset where to start reading from
	private int count = 0; //the number of DI's to read
	private int repeat = 1; //a loop for repeating the transaction
	

	private TCPMasterConnection con = null; //the connection
	private ModbusTCPTransaction trans = null; //the transaction
	private ReadInputDiscretesRequest req = null; //the request
	
	public ClientMaster(String address) throws UnknownHostException {
		addr = InetAddress.getByName(address);
	}
	
	public ClientMaster(String address, int port, int ref, int count, int repeat) throws UnknownHostException {
		this(address);
		this.port = port;
		this.ref = ref;
		this.count = count;
		this.repeat = repeat;
	}
	
	public void setupConnection() throws Exception{
		con = new TCPMasterConnection(addr);
		con.setPort(port);
		con.connect();

		//3. Prepare the request
		req = new ReadInputDiscretesRequest(ref, count);

		//4. Prepare the transaction
		trans = new ModbusTCPTransaction(con);
		trans.setRequest(req);
	}
	
	public void closeConnection(){
		con.close();
	}
	
	public ReadInputDiscretesResponse getObservation() throws ModbusIOException, ModbusSlaveException, ModbusException{
		trans.execute();
		return (ReadInputDiscretesResponse) trans.getResponse();
	}
	
	public static void main(String[] args) {
		String address = "192.168.1.100";
		int port = 5555;
		int ref = 0;
		int count = 4;
		int repeat = 3;
		try {
			ClientMaster clientMaster = new ClientMaster(address, port, ref, count, repeat);
			clientMaster.setupConnection();
			for (int i = 0; i < 50; i++) {
				ReadInputDiscretesResponse response =  clientMaster.getObservation();
				System.out.println(response.getDiscretes().toString());
				clientMaster.closeConnection();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
